package kz.attractorschool.moviereviewrr.repository;

import kz.attractorschool.moviereviewrr.model.Review;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Repository
public interface ReviewRepository extends CrudRepository<Review, String> {

    public Iterable<Review> findAllByMovieId(String id);

    public Iterable<Review> findReviewByMovieId(String id);

}
