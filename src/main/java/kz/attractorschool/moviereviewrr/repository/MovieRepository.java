package kz.attractorschool.moviereviewrr.repository;

import kz.attractorschool.moviereviewrr.model.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends CrudRepository<Movie, String> {

    public Iterable<Movie> findAll(Sort s);

    public Iterable<Movie> findAllByReleaseYearGreaterThanEqual(int year, Sort s);

    Iterable<Movie> findAllByReleaseYearBetween(int year, int year2, Sort s);

    @Query("{'releaseYear' : { '$gte' : ?0, '$lte' : ?1 }}")
    public Iterable<Movie> getMoviesBetween(int year, int year2, Sort s);

    public Iterable<Movie> findByTitle(String title);

    public Iterable<Movie> findByActors(String actors);

    public Iterable<Movie> findByDirectors(String directors);

    public Iterable<Movie> findAllByRatingEquals(double rating);

    Page<Movie> findAllBy(Pageable pageable);




}
