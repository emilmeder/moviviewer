package kz.attractorschool.moviereviewrr.controller;
import kz.attractorschool.moviereviewrr.model.Movie;
import kz.attractorschool.moviereviewrr.model.Review;
import kz.attractorschool.moviereviewrr.model.User;
import kz.attractorschool.moviereviewrr.repository.MovieRepository;
import kz.attractorschool.moviereviewrr.repository.ReviewRepository;
import kz.attractorschool.moviereviewrr.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Controller {
    @Autowired
    MovieRepository mr;
    @Autowired
    ReviewRepository rr;
    @Autowired
    UserRepository ur;

    @GetMapping("/movie")
    public Iterable<Movie> getMovie() {
        Sort s = Sort.by(Sort.Order.asc("title"));
        return mr.findAll(s);
    }

    @GetMapping("/movienew/{year}")
    public Iterable<Movie> getMovie(@PathVariable("year") int year) {
        Sort s = Sort.by(Sort.Order.asc("title"));
        return mr.findAllByReleaseYearGreaterThanEqual(year, s);
    }

    @GetMapping("/movienew/{year}/{year2}")
    public Iterable<Movie> getMovie(@PathVariable("year") int year,
                                    @PathVariable("year2") int year2) {
        Sort s = Sort.by(Sort.Order.asc("title"));
        return mr.findAllByReleaseYearBetween(year, year2, s);
    }

    @GetMapping("/moviebetween/{year}/{year2}")
    public Iterable<Movie> getMovieBetween(@PathVariable("year") int year,
                                           @PathVariable("year2") int year2) {
        Sort s = Sort.by(Sort.Order.asc("title"));

        return mr.getMoviesBetween(year, year2, s);
    }

    @GetMapping("/movieByName/{title}")
    public Iterable<Movie> getMovie(@PathVariable("title") String title) {
        return mr.findByTitle(title);
    }

    @GetMapping("/movieByActor/{actor}")
    public Iterable<Movie> getMovieByActor(@PathVariable("actor") String actor) {
        return mr.findByActors(actor);
    }

    @GetMapping("/movieByDirector/{director}")
    public Iterable<Movie> findByDirectors(@PathVariable("director") String director) {
        return mr.findByDirectors(director);
    }

    @GetMapping("/userByEmail/{email}")
    public Iterable<User> findByEmail(@PathVariable("email") String email) {
        return ur.findByEmail(email);
    }


    @GetMapping("/userByName/{name}")
    public Iterable<User> findByName(@PathVariable("name") String name) {
        return ur.findByName(name);
    }

    @GetMapping("/movieByRating/{rating}")
    public Iterable<Movie> findAllByRating(@PathVariable("rating") double rating) {
        return mr.findAllByRatingEquals(rating);
    }


    @GetMapping("/reviewAllByMovieId/{id}")
    public Iterable<Review> findAllByMovieId(@PathVariable("id") String id) {
        return rr.findAllByMovieId(id);
    }

    @GetMapping("/reviewByMovieId/{id}")
    public Iterable<Review> findReviewByMovieId(@PathVariable("id") String id) {
        return rr.findReviewByMovieId(id);
    }

    @GetMapping("/sortMovieByName")
    public Page<Movie> findAllByName(){
        Sort sortBy = Sort.by(Sort.Order.asc("title"));
        int page = 1;
        int count = 10;
        Pageable pageable = PageRequest.of(page, count, sortBy);
        Page<Movie> result = mr.findAllBy(pageable);
        return result;
    }

    @GetMapping("/sortMovieByRating")
    public Page<Movie> findAllByRating(){
        Sort sortBy = Sort.by(Sort.Order.asc("rating"));
        int page = 1;
        int count = 10;
        Pageable pageable = PageRequest.of(page, count, sortBy);
        Page<Movie> result = mr.findAllBy(pageable);
        return result;
    }

}
